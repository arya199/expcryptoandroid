package com.sitc;

import java.net.URLEncoder;
import java.security.AlgorithmParameters;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Crypto {
	private static final String AES_KEY = "oPTsCSpBR13CDoA4";
	private static final String SERIAL_NUMBER = "ea1cdc0d-5dcb-4c92-93b6-664c649c831d";
	
	private IvParameterSpec privIv;
	
	private String ivParam;
	
	public Crypto() {}
	
	public String encryptAesCbc(String source) {
		String encryptedStr;
		
		byte[] data = AES_KEY.getBytes();
		byte[] sourceBytes = reconstractKey(SERIAL_NUMBER).getBytes();
		
		SecretKeySpec sKey = new SecretKeySpec(AES_KEY.getBytes(), "AES");
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, sKey);
			privIv = cipher.getParameters().getParameterSpec(IvParameterSpec.class);
			byte[] encoded = cipher.getParameters().getEncoded();
			ivParam = Base64.encodeBytes(encoded);
			//ivParam = Base64.encodeBytes(privIv.getIV());
			
			byte[] encrypted = cipher.doFinal(sourceBytes);
			encryptedStr = Base64.encodeBytes(encrypted);
//			encryptedStr = new String(encrypted);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return encryptedStr;
	}
	
	public String decryptAesCbc(String source) {
		String msg = "fail";
		
		Key sKey = new SecretKeySpec(AES_KEY.getBytes(), "AES");
		
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] test = Base64.decode(ivParam);
			byte[] test2 = new byte[16];
			for (int i = 0; i < test.length; i++) {
				System.out.println(">" + i + ". " + test[i]);
			}
			for (int i = 2; i < test.length; i++) {
				test2[i-2] = test[i];
			}
			for (int i = 0; i < test2.length; i++) {
				System.out.println(i + ". " + test2[i]);
			}
			cipher.init(Cipher.DECRYPT_MODE, sKey, new IvParameterSpec(test2));
			
			byte[] decodedValue = Base64.decode(source);
//			byte[] decodedValue = source.getBytes();
			
			msg = new String(cipher.doFinal(decodedValue));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return msg;
	}
	
	public String decryptAesCbcFromPlainString(String source) {
		String msg = "fail";
		
		Key sKey = new SecretKeySpec(AES_KEY.getBytes(), "AES");
		
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
//			IvParameterSpec iv = new IvParameterSpec(cipher.getParameters().getEncoded());
			cipher.init(Cipher.DECRYPT_MODE, sKey, privIv);
			
			byte[] decodedValue = Base64.decode(source);
			msg = new String(cipher.doFinal(decodedValue));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return msg;
	}
	
	public String decryptAesCbcWithIv(String source, String iv) {
		String msg = "fail";
		
		Key sKey = new SecretKeySpec(AES_KEY.getBytes(), "AES");
		
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			byte[] test = Base64.decode(iv);
			byte[] test2 = new byte[16];
			for (int i = 2; i < test.length; i++) {
				test2[i-2] = test[i];
			}
			IvParameterSpec ivSpec = new IvParameterSpec(test2);
			cipher.init(Cipher.DECRYPT_MODE, sKey, ivSpec);
			
			byte[] decodedValue = Base64.decode(source);
			msg = new String(cipher.doFinal(decodedValue));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		 
		return msg;
	}
	
	public String getIvParam() {
		return ivParam;
	}
	
	public static String reconstractKey(String str) {
        StringBuilder suffled = new StringBuilder();
        addDevideByX(suffled, str, 3);
        addDevideByX(suffled, str, 13);
        addDevideByX(suffled, str, 7);
        addDevideByX(suffled, str, 11);
        addDevideByX(suffled, str, 5);

        return suffled.toString();
    }
	
	/**
     * strのxで割り切れる位置に存在する文字列だけをsbに追加する
     * @param sb 追加するバッファ
     * @param str 文字列
     * @param x 割る数
     */
    private static void addDevideByX(StringBuilder sb, String str, int x) {
        for (int i = 0; i < str.length(); i++) {
            if (i % x == 0) {
                sb.append(str.charAt(i));
            }
        }
    }
}

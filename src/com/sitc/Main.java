package com.sitc;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class Main {

	public static void main(String[] args) {
		Crypto crypto = new Crypto();
//		crypto.encryptAesCbc("encrypt me");
		
		// Tested shift
		System.out.println(Crypto.reconstractKey("756b3335-37ed-4d68-a3b2-2d2ae90614cf"));
		
//		String encrypted = crypto.encryptAesCbc("encrypt me");
//		System.out.println("Encrypted: " + encrypted + " with iv " + crypto.getIvParam()) ;
		
//		String decrypted = crypto.decryptAesCbc(crypto.encryptAesCbc("encrypt me"));
//		System.out.println("Decrypted: " + decrypted);
//		
////		String decryptedEx = crypto.decryptAesCbc("d7ZEMOW1w3J/K+Vul4Fp3Q==");
//		String decryptedEx = crypto.decryptAesCbcWithIv("3tx6Txl7uGfgIUgGablBpg==", "KvorYJ/6SFKL74SIXlRIOg==");
//		System.out.println("Decrypted with provided iv: " + decryptedEx);
		
		// From iPhone
		String decryptedExPhone = crypto.
				decryptAesCbcWithIv("VGOVzyYxEFfiK0ac+z03nOHD1KAdOKL3rDrvpQ9JCpr0BP183IiksC10Csh+C3LQ", 
						"Z2OXzqvrzgW76G8pyWa0Wg==");
		System.out.println("Decrypted with provided iv: " + decryptedExPhone);
		
//		try {
//			System.out.println(URLEncoder.encode("mIPeXjlX6cE0Lrxol0SKWg==:mKkcWcjj2kHc/ESQUo9TqNyM3l1VUwUJE896icYU/Gdk9NpsmZ0OojnU+LlhcLsO", "UTF-8"));
//		} catch (UnsupportedEncodingException e) {
//			e.printStackTrace();
//		}
	}
}
